Books are increasingly becoming digital. New challenges and opportunities arises with this trend.
In this project we aim to build a system where digital books behave more or less like their physical counterparts,
while retaining the ease of use and environmental benefits of the digital world.
The current systems treat every digital copy of a given book as the same, and each of the copies
are indistinguishable between each other (eg. Kindle store).
We aim to make each and every digital copy unique, the way each and every physical copies are unique.
This brings digital scarcity, and new sets of behaviors and applications arrive with it.
In our system, a user who buys the book is the OWNER of the book, and he/she is able to sell/give the copy
to whomsoever, the same way one is free to give physical books to friends and family. So a book bought and read can be sold
to someone else as resale, generating revenue for the owner, the book publisher gets a cut of this transaction as well,
making it an additional stream of revenue for the publisher.
An entirely new market can be dug up from this, due to the uniqueness of the copies. Copies can be auctioned off,
and users might be interested in paying a premium for certain copies (copy #1 of Fifty Shades of Grey, for example).

Books are just a single use-case, of course. Any kind of digital content(movies, music, artwork) etc. can be made unique like this.